//Udacity HW 4
//Radix Sorting

#include <stdio.h>
#include "utils.h"
#include <thrust/host_vector.h>

/* Red Eye Removal
   ===============

   For this assignment we are implementing red eye removal.  This is
   accomplished by first creating a score for every pixel that tells us how
   likely it is to be a red eye pixel.  We have already done this for you - you
   are receiving the scores and need to sort them in ascending order so that we
   know which pixels to alter to remove the red eye.

   Note: ascending order == smallest to largest

   Each score is associated with a position, when you sort the scores, you must
   also move the positions accordingly.

   Implementing Parallel Radix Sort with CUDA
   ==========================================

   The basic idea is to construct a histogram on each pass of how many of each
   "digit" there are.   Then we scan this histogram so that we know where to put
   the output of each digit.  For example, the first 1 must come after all the
   0s so we have to know how many 0s there are to be able to start moving 1s
   into the correct position.

   1) Histogram of the number of occurrences of each digit
   2) Exclusive Prefix Sum of Histogram
   3) Determine relative offset of each digit
        For example [0 0 1 1 0 0 1]
                ->  [0 1 0 1 2 3 2]
   4) Combine the results of steps 2 & 3 to determine the final
      output location for each element and move it there

   LSB Radix sort is an out-of-place sort and you will need to ping-pong values
   between the input and output buffers we have provided.  Make sure the final
   sorted results end up in the output buffer!  Hint: You may need to do a copy
   at the end.

 */

__global__ void prefixsum_kernel(unsigned int* d_out, unsigned int* d_in, const int n) {

 extern __shared__ float temp[];

 int offset = 1;
 temp[2 * threadIdx.x] = d_in[2 * threadIdx.x];
 temp[2 * threadIdx.x + 1] = d_in[2 * threadIdx.x + 1];

 for (int d = n>>1; d > 0; d >>= 1) {

   __syncthreads();

   if (threadIdx.x < d) {
     int ai = offset * (2 * threadIdx.x + 1) - 1;
     int bi = offset * (2 * threadIdx.x + 2) - 1;
     temp[bi] += temp[ai];
   }

   offset *= 2;
 }

 // clear the last element
 if (threadIdx.x == 0) {
   temp[n - 1] = 0;
 }

 // traverse down tree & build scan
 for (int d = 1; d < n; d *= 2) {

   offset >>= 1;
   __syncthreads();

   if (threadIdx.x < d) {
     int ai = offset * (2 * threadIdx.x + 1) - 1;
     int bi = offset * (2 * threadIdx.x + 2) - 1;
     float t = temp[ai];
     temp[ai] = temp[bi];
     temp[bi] += t;
   }
 }

 __syncthreads();
 // write result to global memory
 d_out[2 * threadIdx.x] = temp[2 * threadIdx.x];
 d_out[2 * threadIdx.x + 1] = temp[2 * threadIdx.x + 1];
}


__global__ void histo_kernel(unsigned int* d_out, unsigned int* const d_in,
                             unsigned int mask, unsigned int position, const size_t numElems) {
  int myId = threadIdx.x + blockDim.x * blockIdx.x;
  if (myId >= numElems) { return; }
  unsigned int bin = (d_in[myId] & mask) >> position;
  atomicAdd(&(d_out[bin]), 1);
}

__global__ void map_kernel(unsigned int*       vals_dst,
                           unsigned int*       pos_dst,
                           unsigned int*       vals_src,
                           unsigned int*       pos_src,
                           const size_t        numElems,
                           unsigned int        mask,
                           unsigned int        bitOffset,
                           unsigned int*       d_scan) {
  for (unsigned int i  = 0; i < numElems; i++) {
    unsigned int bin = (vals_src[i] & mask) >> bitOffset;
    vals_dst[d_scan[bin]] = vals_src[i];
    pos_dst[d_scan[bin]] = pos_src[i];
    d_scan[bin]++;
  }
}

void your_sort( unsigned int* const d_inputVals,
                unsigned int* const d_inputPos,
                unsigned int* const d_outputVals,
                unsigned int* const d_outputPos,
                const    size_t     numElems) {

  const int maxThreadsPerBlock = 1024;
  int threads = maxThreadsPerBlock;
  int blocks = (numElems + maxThreadsPerBlock - 1) / maxThreadsPerBlock;
  printf("num elements: %d, threads / block: %d, num blocks: %d\n",
          numElems, threads, blocks);

  const int numBits = 4;
  const int numBins = 1 << numBits;
  const int bin_bytes = sizeof(unsigned int) * numBins;
  const int elm_bytes = sizeof(unsigned int) * numElems;
  printf("numBits: %d, numBins: %d\n", numBits, numBins);


  unsigned int* d_bins;
  unsigned int* d_scan;
  checkCudaErrors(cudaMalloc((void **) &d_bins, bin_bytes));
  checkCudaErrors(cudaMalloc((void **) &d_scan, bin_bytes));

  unsigned int *vals_src = d_inputVals;
  unsigned int *pos_src  = d_inputPos;
  unsigned int *vals_dst = d_outputVals;
  unsigned int *pos_dst = d_outputPos;

  for (unsigned int i = 0; i < 8 * sizeof(unsigned int); i+= numBits) {
    printf("iteration %d, vals_src=%p, vals_dst=%p\n", i, (void*)vals_src, (void*)vals_dst);
    unsigned int mask = (numBins - 1) << i;

    checkCudaErrors(cudaMemset(d_bins, 0, bin_bytes));
    checkCudaErrors(cudaMemset(d_scan, 0, bin_bytes));

    // histogram the number of occurences of each digit
    histo_kernel<<<blocks, threads>>>(d_bins, vals_src, mask, i, numElems);
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());// histogram the number of occurences of each digit

    // exclusive prefix sum of the histogram, so that we know the
    // starting location of each bin
    prefixsum_kernel<<<1, numBins / 2, bin_bytes>>>(d_scan, d_bins, numBins);
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

    // DEBUG OUTPUT
    // unsigned int h_bins[numBins];
    // unsigned int h_scan[numBins];
    // checkCudaErrors(cudaMemcpy(h_bins, d_bins, bin_bytes, cudaMemcpyDeviceToHost));
    // checkCudaErrors(cudaMemcpy(h_scan, d_scan, bin_bytes, cudaMemcpyDeviceToHost));
    // printf("  bins: ");
    // for (int j = 0; j < numBins; j++) {
    //   printf("[%-2d]=%-6d  ", j, h_bins[j]);
    // }
    // printf("\n");
    // printf("  scan: ");
    // for (int j = 0; j < numBins; j++) {
    //   printf("[%-2d]=%-6d  ", j, h_scan[j]);
    // }
    // printf("\n");

    // TODO: improve this!
    // determine relative offset of each digit
    map_kernel<<<1, 1>>>(vals_dst, pos_dst, vals_src, pos_src, numElems, mask, i, d_scan);
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

    // swap pointers
    std::swap(vals_dst, vals_src);
    std::swap(pos_dst, pos_src);

  }

  // need to copy from input buffer into output
  checkCudaErrors(cudaMemcpy(d_outputVals, d_inputVals, elm_bytes, cudaMemcpyDeviceToDevice));
  checkCudaErrors(cudaMemcpy(d_outputPos, d_inputPos, elm_bytes, cudaMemcpyDeviceToDevice));
  printf("cp %p -> %p\n", (void*) d_inputVals, (void*) d_outputVals);

  // free memory
  cudaFree(d_bins);
  cudaFree(d_scan);
}
