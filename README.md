Udacity: Introduction to Parallel Programming
=============================
Project files for the Introduction to Parallel Programming on Udacity: https://www.udacity.com/course/cs344

Course repository: https://github.com/udacity/cs344

### CUDA/OpenCL on Arch
* Install CUDA via AUR: `cuda` (https://www.archlinux.org/packages/community/x86_64/cuda/)
* Install OpenCV via AUR: `opencv` (https://www.archlinux.org/packages/extra/x86_64/opencv/)
```sh
OPENCV_LIBPATH=/usr/lib
OPENCV_INCLUDEPATH=/usr/include
OPENCV_LIBS=-lopencv_core -lopencv_imgproc -lopencv_highgui
CUDA_INCLUDEPATH=/opt/cuda/include
```


