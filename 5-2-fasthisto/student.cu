/* Udacity HW5
   Histogramming for Speed

   The goal of this assignment is compute a histogram
   as fast as possible.  We have simplified the problem as much as
   possible to allow you to focus solely on the histogramming algorithm.

   The input values that you need to histogram are already the exact
   bins that need to be updated.  This is unlike in HW3 where you needed
   to compute the range of the data and then do:
   bin = (val - valMin) / valRange to determine the bin.

   Here the bin is just:
   bin = val

   so the serial histogram calculation looks like:
   for (i = 0; i < numElems; ++i)
     histo[val[i]]++;

   That's it!  Your job is to make it run as fast as possible!

   The values are normally distributed - you may take
   advantage of this fact in your implementation.

*/

#include <stdio.h>

#include "utils.h"
#include "timer.h"

__global__
void sharedMemHisto(const unsigned int* const vals, //INPUT
                    unsigned int* const histo,      //OUPUT
                    int numBins, int numVals)
{
  extern __shared__ int localHistogram[];
  
  // fill local histogram with 0s
  if (threadIdx.x < numBins) {
    localHistogram[threadIdx.x] = 0;
  }
  __syncthreads();
  
  // perform histogram computation in shared memory
  unsigned int idx = blockDim.x * blockIdx.x + threadIdx.x;
  atomicAdd(&(localHistogram[vals[idx]]), 1);
  
  
  __syncthreads();
  
  // write result back to global memory
  if (threadIdx.x < numBins) {
    atomicAdd(&(histo[threadIdx.x]), localHistogram[threadIdx.x]);
  }
}

__global__
void simpleHisto(const unsigned int* const vals, //INPUT
               unsigned int* const histo,      //OUPUT
               int numVals)
{
  unsigned int idx = blockDim.x * blockIdx.x + threadIdx.x;
  if (idx < numVals) {
    atomicAdd(&(histo[vals[idx]]), 1);   
  }
}

void computeHistogram(const unsigned int* const d_vals, //INPUT
                      unsigned int* const d_histo,      //OUTPUT
                      const unsigned int numBins,
                      const unsigned int numElems)
{
  // prepare kernel
  dim3 dimBlock(numBins);
  dim3 dimGrid((numElems - 1) / dimBlock.x + 1);
  printf("numBins: %d, numElems: %d\n", numBins, numElems);
  printf("dimBlock: %d, dimGrid: %d\n", dimBlock.x, dimGrid.x);

  //simpleHisto<<<dimGrid, dimBlock>>>(d_vals, d_histo, numElems);
  sharedMemHisto<<<dimGrid, dimBlock, numBins * sizeof(int)>>>(d_vals, d_histo, numBins, numElems);

  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}
