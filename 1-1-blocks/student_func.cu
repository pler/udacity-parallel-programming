// Coursera - Introduction to Parallel Programming
// Lesson 1 - Homework 1: Color to Greyscale Conversion

//To convert an image from color to grayscale one simple method is to
//set the intensity to the average of the RGB channels.  But we will
//use a more sophisticated method that takes into account how the eye 
//perceives color and weights the channels unequally.

//The eye responds most strongly to green followed by red and then blue.
//The NTSC (National Television System Committee) recommends the following
//formula for color to greyscale conversion:
//I = .299f * R + .587f * G + .114f * B

#include "utils.h"

__global__
void rgba_to_greyscale(const uchar4* const rgbaImage,
                       unsigned char* const greyImage,
                       int numRows, int numCols)
{
  // calculate the offset
  int blockId = (blockIdx.y * gridDim.x) + blockIdx.x;
  int threadId = (threadIdx.y * blockDim.x) + threadIdx.x;
  int globalIdx = (blockId * (blockDim.x * blockDim.y)) + threadId;
  greyImage[globalIdx] = 0.299f * rgbaImage[globalIdx].x + 0.587f * rgbaImage[globalIdx].y + 0.114f * rgbaImage[globalIdx].z;
}
void your_rgba_to_greyscale(const uchar4 * const h_rgbaImage, uchar4 * const d_rgbaImage,
                            unsigned char* const d_greyImage, size_t numRows, size_t numCols)
{
  // for now just one block
  const dim3 blockSize(1, 1, 1);
  const dim3 gridSize(numCols, numRows, 1);
  rgba_to_greyscale<<<gridSize, blockSize>>>(d_rgbaImage, d_greyImage, numRows, numCols);
  
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

}
