/* Udacity Homework 3
   HDR Tone-mapping
*/

#include<stdio.h>
#include "utils.h"


__global__
void minmax_reduce_kernel(float* d_out, const int out_offset, const float* d_in, const int in_offset) {

  // allocated in the kernel call
  extern __shared__ float sdata[];

  int myId = threadIdx.x + blockDim.x * blockIdx.x;

  // load to shared from global memory
  sdata[threadIdx.x] = d_in[myId];
  sdata[threadIdx.x + blockDim.x] = d_in[myId + in_offset];
  __syncthreads();  // make sure the entire block is loaded

  // do reduction
  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
    if (threadIdx.x < s) {
        sdata[threadIdx.x]              = min(sdata[threadIdx.x + s],
                                              sdata[threadIdx.x]);
        sdata[threadIdx.x + blockDim.x] = max(sdata[threadIdx.x + blockDim.x + s],
                                              sdata[threadIdx.x + blockDim.x]);
    }
    __syncthreads();  // make sure all min and max at one stage are done!
  }

  // only thread 0 writes result back to global memory
  if (threadIdx.x == 0) {
      d_out[blockIdx.x] = sdata[0];
      d_out[blockIdx.x + out_offset] = sdata[blockDim.x];
    }
}

__global__
void histo_kernel(unsigned int* d_bins, const float*  d_in,
                  float  min_logLum, float max_logLum, const size_t numBins)  {
  int myId = threadIdx.x + blockDim.x * blockIdx.x;
  float myItem = d_in[myId];
  float logLumRange = max_logLum - min_logLum;
  unsigned int myBin = min(static_cast<unsigned int>((myItem - min_logLum) / logLumRange * numBins),
                           static_cast<unsigned int>(numBins - 1));
  atomicAdd(&(d_bins[myBin]), 1);
}

__global__
void prescan(unsigned int *d_out, unsigned int *d_in, int n) {
  extern __shared__ float temp[];

  int tid = threadIdx.x;
  int offset = 1;

  temp[2 * tid] = d_in[2 * tid];
  temp[2 * tid + 1] = d_in[2 * tid + 1];

  for (int d = n>>1; d > 0; d >>= 1) {
    __syncthreads();
    if (tid < d) {
      int ai = offset * (2 * tid + 1) - 1;
      int bi = offset * (2 * tid + 2) - 1;

      temp[bi] += temp[ai];
    }
    offset *= 2;
  }

  if (tid == 0) {
    temp[n - 1] = 0; // clear the last element
  }

  // traverse down tree & build scan
  for (int d = 1; d < n; d *= 2) {
    offset >>= 1;
    __syncthreads();

    if (tid < d) {
      int ai = offset * (2 * tid + 1) - 1;
      int bi = offset * (2 * tid + 2) - 1;

      float t = temp[ai];
      temp[ai] = temp[bi];
      temp[bi] += t;
    }
  }

  __syncthreads();

  // write result to global memory
  d_out[2 * tid] = temp[2 * tid];
  d_out[2 * tid + 1] = temp[2 * tid + 1];
}

// ----------------------------------------------------------------------------


void findMinMax(const float * d_in, float &min_logLum, float &max_logLum, int blocks, int threads) {
  // declare GPU memory pointer and allocate memory on the GPU
  float *d_minmax, *d_out;
  // NOTE: min and max are kept in the same array, so the output is twice the needed size
  checkCudaErrors(cudaMalloc((void **) &d_minmax, 2 * blocks * sizeof(float)));
  minmax_reduce_kernel<<<blocks, threads, 2 * threads * sizeof(float)>>>(d_minmax, blocks, d_in, 0);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  // rerun reduce on the one block that is left
  threads = blocks;
  blocks = 1;
  checkCudaErrors(cudaMalloc((void **) &d_out, 2 * sizeof(float)));
  minmax_reduce_kernel<<<blocks, threads, 2 * threads * sizeof(float)>>>(d_out, 1, d_minmax, threads);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  // copy back to host
  checkCudaErrors(cudaMemcpy(&min_logLum, d_out, sizeof(float), cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpy(&max_logLum, &d_out[1], sizeof(float), cudaMemcpyDeviceToHost));
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  // free memory
  cudaFree(d_minmax);
  cudaFree(d_out);
}

void your_histogram_and_prefixsum(const float* const d_logLuminance,
                                  unsigned int* const d_cdf,
                                  float &min_logLum,
                                  float &max_logLum,
                                  const size_t numRows,
                                  const size_t numCols,
                                  const size_t numBins) {

  const int maxThreadsPerBlock = 1024;
  int threads = maxThreadsPerBlock;
  int blocks = (numRows * numCols + maxThreadsPerBlock - 1) / maxThreadsPerBlock;
  printf("image dimensions: %dx%d\n", numCols, numRows);

  // find the minimum and maximum value in the input logLuminance channel
  findMinMax(d_logLuminance, min_logLum, max_logLum, blocks, threads);
  const float range = abs(max_logLum - min_logLum);
  printf("min: %f, max: %f, range: %f\n", min_logLum, max_logLum, range);

  // declare GPU memory pointer, allocate memory and fill it with 0s
  const int BIN_BYTES = numBins * sizeof(unsigned int);
  unsigned int * d_bins;
  checkCudaErrors(cudaMalloc((void **) &d_bins, BIN_BYTES));
  checkCudaErrors(cudaMemset(d_bins, 0, BIN_BYTES));

  // generate a histogram of all the values in the logLuminance channel
  histo_kernel<<<blocks, threads>>>(d_bins, d_logLuminance,  min_logLum, max_logLum, numBins);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  // calculate the cumulative distribution via excluse prefix sum scan
  threads = numBins / 2;
  blocks = 1;
  prescan<<<blocks, threads, 2 * threads * sizeof(unsigned int)>>>(d_cdf, d_bins, 2 * threads);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  cudaFree(d_bins);
}
