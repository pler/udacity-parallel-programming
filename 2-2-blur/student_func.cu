// Homework 2
// Image Blurring
//
//****************************************************************************

#include "utils.h"
#include<stdio.h>

__global__
void gaussian_blur(const unsigned char* const inputChannel,
                   unsigned char* const outputChannel,
                   int numRows, int numCols,
                   const float* const filter, const int filterWidth)
{

  const int2 absolutePosition = make_int2(blockIdx.x * blockDim.x + threadIdx.x,
                                          blockIdx.y * blockDim.y + threadIdx.y);

  // only index pixels in the bounaries of the image
  if (absolutePosition.x >= numCols || absolutePosition.y >= numRows) { return; }

  // calculate the blur out of the surrounding pixels
  float result = 0.f;
  for (int filter_x = -filterWidth/2; filter_x <= filterWidth/2; ++filter_x) {
    for (int filter_y = -filterWidth/2; filter_y <= filterWidth/2; ++filter_y) {
      // clamp the boundary of the image
      int x = min(max(absolutePosition.x + filter_x, 0), static_cast<int>(numCols - 1));
      int y = min(max(absolutePosition.y + filter_y, 0), static_cast<int>(numRows - 1));

      float pixel_value = static_cast<float>(inputChannel[y * numCols + x]);
      float filter_value = filter[(filter_x + filterWidth/2) * filterWidth + filter_y + filterWidth/2];
      result += pixel_value * filter_value;
    }
  }
  outputChannel[absolutePosition.y * numCols + absolutePosition.x] = static_cast<unsigned char>(result);

}

// this kernel takes in an image represented as a uchar4 and splits
// it into three images consisting of only one color channel each
__global__
void separateChannels(const uchar4* const inputImageRGBA,
                      int numRows,
                      int numCols,
                      unsigned char* const redChannel,
                      unsigned char* const greenChannel,
                      unsigned char* const blueChannel)
{
  const int2 absolutePosition = make_int2(blockIdx.x * blockDim.x + threadIdx.x,
                                          blockIdx.y * blockDim.y + threadIdx.y);

  // only index pixels in the bounaries of the image
  if (absolutePosition.x >= numCols || absolutePosition.y >= numRows) { return; }
  
  // copy RGB information out of the RGBA image, alpha is ignored from now on
  const int absoluteIndex = absolutePosition.y * numCols + absolutePosition.x;
  redChannel[absoluteIndex] = inputImageRGBA[absoluteIndex].x;
  greenChannel[absoluteIndex] = inputImageRGBA[absoluteIndex].y;
  blueChannel[absoluteIndex] = inputImageRGBA[absoluteIndex].z;
}

// this kernel takes in three color channels and recombines them
// into one image.  The alpha channel is set to 255 to represent
// that this image has no transparency.
__global__
void recombineChannels(const unsigned char* const redChannel,
                       const unsigned char* const greenChannel,
                       const unsigned char* const blueChannel,
                       uchar4* const outputImageRGBA,
                       int numRows,
                       int numCols)
{
  const int2 thread_2D_pos = make_int2( blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y);

  const int thread_1D_pos = thread_2D_pos.y * numCols + thread_2D_pos.x;

  //make sure we don't try and access memory outside the image
  //by having any threads mapped there return early
  if (thread_2D_pos.x >= numCols || thread_2D_pos.y >= numRows)
    return;

  unsigned char red   = redChannel[thread_1D_pos];
  unsigned char green = greenChannel[thread_1D_pos];
  unsigned char blue  = blueChannel[thread_1D_pos];

  uchar4 outputPixel = make_uchar4(red, green, blue, 255);

  outputImageRGBA[thread_1D_pos] = outputPixel;
}

unsigned char *d_red, *d_green, *d_blue;
float         *d_filter;

void allocateMemoryAndCopyToGPU(const size_t numRowsImage, const size_t numColsImage,
                                const float* const h_filter, const size_t filterWidth)
{

  // allocate memory for the three different channels
  checkCudaErrors(cudaMalloc(&d_red,   sizeof(unsigned char) * numRowsImage * numColsImage));
  checkCudaErrors(cudaMalloc(&d_green, sizeof(unsigned char) * numRowsImage * numColsImage));
  checkCudaErrors(cudaMalloc(&d_blue,  sizeof(unsigned char) * numRowsImage * numColsImage));

  // allocate memory for the filter on the GPU... 
  int bytes = sizeof(float) * filterWidth * filterWidth;
  checkCudaErrors(cudaMalloc(&d_filter, bytes));
  // ... and copy the filter from the host to the device
  checkCudaErrors(cudaMemcpy(d_filter, h_filter, bytes, cudaMemcpyHostToDevice));

 }

void your_gaussian_blur(const uchar4 * const h_inputImageRGBA, uchar4 * const d_inputImageRGBA,
                        uchar4* const d_outputImageRGBA, const size_t numRows, const size_t numCols,
                        unsigned char *d_redBlurred, 
                        unsigned char *d_greenBlurred, 
                        unsigned char *d_blueBlurred,
                        const int filterWidth)
{
  // threads per block
  const dim3 blockSize = dim3(16, 16);
  // number of blocks per thread
  const dim3 gridSize = dim3(numCols / blockSize.x + 1, numRows / blockSize.y + 1);

  printf("image dimensions: %dx%d\n", numCols, numRows);
  printf("blockSize: %dx%d, gridSize: %dx%d\n", blockSize.x, blockSize.y, gridSize.x, gridSize.y);

  // launch kernel to separate RGBA channels
  separateChannels<<<gridSize, blockSize>>>(d_inputImageRGBA,
                                            numRows,
                                            numCols,
                                            d_red,
                                            d_green,
                                            d_blue);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
  
  // call filter for each channel seperately
  gaussian_blur<<<gridSize, blockSize>>>(d_red, d_redBlurred, numRows, numCols, d_filter, filterWidth);
  gaussian_blur<<<gridSize, blockSize>>>(d_green, d_greenBlurred, numRows, numCols, d_filter, filterWidth);
  gaussian_blur<<<gridSize, blockSize>>>(d_blue, d_blueBlurred, numRows, numCols, d_filter, filterWidth);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  // Now we recombine your results. We take care of launching this kernel for you.
  //
  // NOTE: This kernel launch depends on the gridSize and blockSize variables,
  // which you must set yourself.
  recombineChannels<<<gridSize, blockSize>>>(d_redBlurred,
                                             d_greenBlurred,
                                             d_blueBlurred,
                                             d_outputImageRGBA,
                                             numRows,
                                             numCols);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

}


//Free all the memory that we allocated
void cleanup() {
  checkCudaErrors(cudaFree(d_red));
  checkCudaErrors(cudaFree(d_green));
  checkCudaErrors(cudaFree(d_blue));
  checkCudaErrors(cudaFree(d_filter));
}
